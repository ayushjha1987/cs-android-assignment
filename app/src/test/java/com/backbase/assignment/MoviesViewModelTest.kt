package com.backbase.assignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.backbase.assignment.ui.model.*
import com.backbase.assignment.ui.repository.MainRepository
import com.backbase.assignment.ui.test.MainCoroutineScopeRule
import com.backbase.assignment.ui.utils.Resource
import com.backbase.assignment.ui.utils.Resource.Companion.success
import com.backbase.assignment.ui.utils.Status
import com.backbase.assignment.ui.viewmodel.DetailsViewModel
import com.backbase.assignment.ui.viewmodel.MainViewModel
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class MoviesViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineScope = MainCoroutineScopeRule()

    @Mock
    private lateinit var mockObserver: Observer<Resource<Movies>>

    @Mock
    private lateinit var mockDetailsScreen: Observer<Resource<DetailsScreen>>

    private lateinit var mainViewModel: MainViewModel

    private lateinit var detailsViewModel: DetailsViewModel

    @Mock
    private lateinit var useCase: MainRepository

    private lateinit var viewState: Resource<Movies>

    @Captor
    private lateinit var captor: ArgumentCaptor<Resource<Movies>>

    @Captor
    private lateinit var captorDetails: ArgumentCaptor<Resource<DetailsScreen>>


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewState = Resource(Status.LOADING, null, null)
        mainViewModel = MainViewModel(useCase)
        detailsViewModel = DetailsViewModel(useCase)
    }

    @Test
    fun assertNowPlayingMoviesSuccess() {
        coroutineScope.runBlockingTest {
            val movies = Movies(
                listOf(
                    MovieDetails(
                        3020.903,
                        110,
                        false,
                        "/7D430eqZj8y3oVkLFfsWXGRcpEG.jpg",
                        528085,
                        false,
                        "/5UkzNSOK561c2QRy2Zr4AkADzLT.jpg",
                        "en",
                        "2067",
                        null,
                        "2067",
                        5.7,
                        "A lowly utility worker is called to the future by a mysterious radio signal, he must leave his dying wife to embark on a journey that will force him to face his deepest fears in an attempt to change the fabric of reality and save humankind from its greatest environmental crisis yet.",
                        "2020-10-01"
                    )
                ), 1, 1361, null, 69
            )
            val flow = flow {
                emit(success(movies))
            }

            `when`(useCase.getNowPlayingMovies()).thenReturn(flow)
            val liveData = mainViewModel.getNowPlayingMovies()
            liveData.observeForever(mockObserver)
            verify(mockObserver).onChanged(captor.capture())
            assertEquals(
                1361, captor.value.data?.totalResults
            )

            assertEquals(
                true, captor.value.data?.results?.size!! > 0
            )
        }
    }

    @Test
    fun assertFailureForNowPlaying() {
        coroutineScope.runBlockingTest {
            val flow = flow {
                emit(Resource.error(null, "error occurred"))
            }
            `when`(useCase.getNowPlayingMovies()).thenReturn(flow)
            val liveData = mainViewModel.getNowPlayingMovies()
            liveData.observeForever(mockObserver)
            verify(mockObserver).onChanged(captor.capture())
            assertEquals(
                null, captor.value.data
            )
        }
    }

    @Test
    fun assertGetMovieDetails() {
        coroutineScope.runBlockingTest {
            val detailsScreen = DetailsScreen(
                false,
                "/5UkzNSOK561c2QRy2Zr4AkADzLT.jpg",
                "null",
                0,
                listOf(Genres(878, "Science Fiction"), Genres(53, "Thriller")),
                "http://screen.nsw.gov.au/project/2067",
                528085,
                "tt1918734",
                "en",
                "2067",
                "A lowly utility worker is called to the future by a mysterious radio signal, he must leave his dying wife to embark on a journey that will force him to face his deepest fears in an attempt to change the fabric of reality and save humankind from its greatest environmental crisis yet.",
                3020.903,
                "/7D430eqZj8y3oVkLFfsWXGRcpEG.jpg",
                listOf(ProductionCompanies(1, "", "", "")),
                listOf(ProductionCountries("", "")),
                "2067",
                1,
                120,
                ArrayList(),
                "5.8",
                "no tagline",
                "",
                false,
                5.7,
                100
            )
            val flow = flow {
                emit(success(detailsScreen))
            }

            `when`(useCase.getMovieDetails(1)).thenReturn(flow)
            val liveData = detailsViewModel.getMovieDetails(1)
            liveData.observeForever(mockDetailsScreen)
            verify(mockDetailsScreen).onChanged(captorDetails.capture())
            assertEquals(
                "en", captorDetails.value.data?.original_language
            )

            assertEquals(
                false, captorDetails.value.data?.adult
            )

            assertEquals(
                true, captorDetails.value.data?.genres?.size!! > 0
            )
        }
    }

    @Test
    fun assertFailureMovieDetails() {
        coroutineScope.runBlockingTest {
            val flow = flow {
                emit(Resource.error(null, "error occurred"))
            }
            `when`(useCase.getMovieDetails(1)).thenReturn(flow)
            val liveData = detailsViewModel.getMovieDetails(1)
            liveData.observeForever(mockDetailsScreen)
            verify(mockDetailsScreen).onChanged(captorDetails.capture())
            assertEquals(
                null, captorDetails.value.data
            )
        }
    }
}