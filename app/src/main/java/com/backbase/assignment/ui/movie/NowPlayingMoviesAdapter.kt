package com.backbase.assignment.ui.movie

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.custom.RatingView
import com.backbase.assignment.ui.model.MovieDetails
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class NowPlayingMoviesAdapter() : RecyclerView.Adapter<NowPlayingMoviesAdapter.ViewHolder>() {

    private val movieList: ArrayList<MovieDetails> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_item_horizontal, parent, false)
        )
    }

    fun addMovies(movieDetails: List<MovieDetails>) {
        this.movieList.apply {
            addAll(movieDetails)
        }
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        movieList[i].let { viewHolder.bind(it) }
    }

    override fun getItemCount() = movieList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var poster: ImageView

        fun bind(item: MovieDetails) = with(itemView) {
            poster = itemView.findViewById(R.id.poster)
            Glide.with(poster).load("https://image.tmdb.org/t/p/original/${item.posterPath}")
                .diskCacheStrategy(
                    DiskCacheStrategy.RESOURCE
                ).into(poster)
        }
    }
}