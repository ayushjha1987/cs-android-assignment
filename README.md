Approach used:

1. Have used MVVM pattern to develop the screens
2. Used Retrofit, Hilt Dependency Injection, Glide
3. Used Glide for image caching, used that as the only 3rd party library
4. For the rating view I created a layout for the custom view and created functions to set the text and the color of the progress bar
