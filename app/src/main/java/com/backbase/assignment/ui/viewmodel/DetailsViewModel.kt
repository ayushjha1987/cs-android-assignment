package com.backbase.assignment.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.backbase.assignment.ui.model.DetailsScreen
import com.backbase.assignment.ui.repository.MainRepository
import com.backbase.assignment.ui.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect


class DetailsViewModel @ViewModelInject constructor(private val mainRepository: MainRepository) : ViewModel(), LifecycleObserver {

    var movieDetail: MutableLiveData<Resource<DetailsScreen>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    suspend fun getMovieDetails(id: Int): LiveData<Resource<DetailsScreen>> {
        mainRepository.getMovieDetails(id).collect {
            movieDetail.value = it
        }
        return movieDetail
    }
}