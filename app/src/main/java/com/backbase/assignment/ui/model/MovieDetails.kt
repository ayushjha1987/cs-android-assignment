package com.backbase.assignment.ui.model

import com.google.gson.annotations.SerializedName

data class MovieDetails(
    var popularity: Double? = null,
    var voteCount: Int? = null,
    var video: Boolean? = null,
    @SerializedName("poster_path")
    var posterPath: String? = null,
    var id: Int? = null,
    var adult: Boolean? = null,
    var backdropPath: String? = null,
    var originalLanguage: String? = null,
    var originalTitle: String? = null,
    var genreIds: List<Int>? = null,
    var title: String? = null,
    @SerializedName("vote_average")
    var voteAverage: Double? = null,
    var overview: String? = null,
    var release_date: String? = null
)


