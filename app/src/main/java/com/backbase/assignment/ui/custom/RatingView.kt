package com.backbase.assignment.ui.custom

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.backbase.assignment.R

class RatingView(context: Context, attrs: AttributeSet?) : RelativeLayout(context, attrs) {
    private var layout: RelativeLayout? = null
    var progressBar: ProgressBar? = null
    var textProgress: TextView? = null

    init {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.RatingView)
        val progressBarRatingView = a.getInt(R.styleable.RatingView_progressBar, 0)
        val textProgressRatingView = a.getString(R.styleable.RatingView_txtProgress)
        val service: String = Context.LAYOUT_INFLATER_SERVICE
        val li = getContext().getSystemService(service) as LayoutInflater
        layout = li.inflate(R.layout.rating_view, this, true) as RelativeLayout?
        progressBar = layout?.findViewById<View>(R.id.progressBar) as ProgressBar
        textProgress = layout?.findViewById<View>(R.id.txtProgress) as TextView
        progressBar?.progress = progressBarRatingView
        textProgress?.text = textProgressRatingView
        a.recycle()
    }

    fun setTextProgress(text: String?) {
        textProgress?.text = text
    }

    fun setProgressBarProgress(progress: Int) {
        progressBar?.progress = progress
    }

    fun setProgressBarDrawable(below50: Boolean) {
        if(below50){
            progressBar?.progressDrawable =
                ContextCompat.getDrawable(context, R.drawable.custom_progress_drawable_yellow)
        }else {
            progressBar?.progressDrawable =
                ContextCompat.getDrawable(context, R.drawable.custom_progress_drawable)
        }
    }
}