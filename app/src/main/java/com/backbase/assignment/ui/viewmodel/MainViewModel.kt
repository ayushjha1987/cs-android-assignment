package com.backbase.assignment.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.backbase.assignment.ui.model.Movies
import com.backbase.assignment.ui.repository.MainRepository
import com.backbase.assignment.ui.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect


class MainViewModel @ViewModelInject constructor(private val mainRepository: MainRepository) : ViewModel(), LifecycleObserver {

    var popularMovies: MutableLiveData<Resource<Movies>> = MutableLiveData()
    var nowPlayingMovies: MutableLiveData<Resource<Movies>> = MutableLiveData()
    private var PAGE_SIZE = 20
    var isLastPageInList: Boolean = false

    @ExperimentalCoroutinesApi
    suspend fun getNowPlayingMovies(): LiveData<Resource<Movies>> {
        mainRepository.getNowPlayingMovies().collect {
            nowPlayingMovies.value = it
        }
        return nowPlayingMovies
    }

    @ExperimentalCoroutinesApi
    suspend fun getPopularMovies(page: Int): LiveData<Resource<Movies>> {
        mainRepository.getPopularMovies(page + 1).collect {
            popularMovies.value = it
            if (page == it.data?.totalPages?.div(PAGE_SIZE) ?: 0) {
                isLastPageInList = true
            }
        }
        return popularMovies
    }

    fun isLastPage(): Boolean {
        return isLastPageInList
    }
}