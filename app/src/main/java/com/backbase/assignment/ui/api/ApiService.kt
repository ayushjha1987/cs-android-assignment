package com.backbase.assignment.ui.api

import com.backbase.assignment.ui.di.NetworkModule.API_KEY
import com.backbase.assignment.ui.model.DetailsScreen
import com.backbase.assignment.ui.model.Movies
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @Headers("application-type: REST", "Content-Type: application/json")
    @GET("movie/now_playing")
    suspend fun getNowPlayingMovies(@Query("api_key") apiKey: String = API_KEY, @Query("language") language: String = "en-US"): Movies

    @Headers("application-type: REST", "Content-Type: application/json")
    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("page") page: Int = 1, @Query("api_key") apiKey: String = API_KEY, @Query("language") language: String = "en-US"): Movies


    @Headers("application-type: REST", "Content-Type: application/json")
    @GET("movie/{id}")
    suspend fun getMovieDetails(@Path("id") id: Int, @Query("api_key") apiKey: String = API_KEY): DetailsScreen
}