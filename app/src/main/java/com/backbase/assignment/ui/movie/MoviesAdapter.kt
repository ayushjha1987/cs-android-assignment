package com.backbase.assignment.ui.movie

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.custom.RatingView
import com.backbase.assignment.ui.activity.details.DetailsActivity
import com.backbase.assignment.ui.model.MovieDetails
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy


class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private val movieList: ArrayList<MovieDetails> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        )
    }

    fun addMovies(movieDetails: List<MovieDetails>) {
        this.movieList.apply {
            addAll(movieDetails)
        }
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        movieList[i].let { viewHolder.bind(it) }
    }

    override fun getItemCount() = movieList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var poster: ImageView
        lateinit var title: TextView
        lateinit var releaseDate: TextView
        lateinit var rating: RatingView
        lateinit var layout: ConstraintLayout

        fun bind(item: MovieDetails) = with(itemView) {
            val voteAverage = item.voteAverage?.times(10)
            poster = itemView.findViewById(R.id.poster)
            Glide.with(poster).load("https://image.tmdb.org/t/p/original/${item.posterPath}")
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(poster)

            title = itemView.findViewById(R.id.title)
            title.text = item.title

            releaseDate = itemView.findViewById(R.id.releaseDate)
            releaseDate.text = item.release_date

            rating = itemView.findViewById(R.id.rating)
            voteAverage?.toInt()?.let { rating.setProgressBarProgress(it) }
            rating.setTextProgress(voteAverage.toString() + "%")
            if (voteAverage != null) {
                rating.setProgressBarDrawable(voteAverage < 50)
            }

            layout = itemView.findViewById(R.id.layout)
            layout.setOnClickListener {
                itemView.context.startActivity(
                    Intent(
                        itemView.context,
                        DetailsActivity::class.java
                    ).putExtra("MOVIE_ID", item.id)
                )
            }

        }
    }
}