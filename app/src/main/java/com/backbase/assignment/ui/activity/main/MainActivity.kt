package com.backbase.assignment.ui.activity.main

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.model.Movies
import com.backbase.assignment.ui.movie.MoviesAdapter
import com.backbase.assignment.ui.movie.NowPlayingMoviesAdapter
import com.backbase.assignment.ui.utils.RecyclerViewPaginator
import com.backbase.assignment.ui.utils.Resource
import com.backbase.assignment.ui.utils.Status
import com.backbase.assignment.ui.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var moviesAdapter: MoviesAdapter
    private lateinit var nowPlayingMoviesAdapter: NowPlayingMoviesAdapter
    private lateinit var recyclerViewVertical: RecyclerView
    private lateinit var recyclerViewHorizontal: RecyclerView
    private val mainViewModel: MainViewModel by viewModels()
    private var job: Job = Job()

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    @ExperimentalCoroutinesApi
    override fun onResume() {
        super.onResume()
        launch {
            mainViewModel.getPopularMovies(1)
            mainViewModel.getNowPlayingMovies()
        }
        setupObservers()
    }

    @ExperimentalCoroutinesApi
    private fun initView() {
        initVerticalRecyclerView()
        initHorizontalRecyclerView()
    }

    @ExperimentalCoroutinesApi
    private fun initVerticalRecyclerView() {
        recyclerViewVertical = findViewById(R.id.recyclerViewVertical)
        recyclerViewVertical.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewVertical.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        moviesAdapter = MoviesAdapter()
        recyclerViewVertical.adapter = moviesAdapter
        recyclerViewVertical.addOnScrollListener(object : RecyclerViewPaginator(recyclerViewVertical) {
            override val isLastPage: Boolean
                get() = mainViewModel.isLastPage()

            override fun loadMore(start: Int, count: Int) {
                launch {
                    mainViewModel.getPopularMovies(start)
                }
            }
        })
    }

    private fun initHorizontalRecyclerView() {
        recyclerViewVertical = findViewById(R.id.recyclerViewHorizontal)
        recyclerViewVertical.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        nowPlayingMoviesAdapter = NowPlayingMoviesAdapter()
        recyclerViewVertical.adapter = nowPlayingMoviesAdapter
    }

    private fun setupObservers() {
        val popularMovieObserver = Observer<Resource<Movies>> {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerViewVertical.visibility = View.VISIBLE
                        resource?.data?.results?.let { movieList ->
                            moviesAdapter.addMovies(movieList)
                            moviesAdapter.notifyDataSetChanged()
                        }

                    }
                    Status.ERROR -> {
                        Snackbar.make(recyclerViewVertical, "Error Occurred", Snackbar.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {

                    }
                }
            }
        }
        val nowPlayingMoviesObserver = Observer<Resource<Movies>> {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerViewVertical.visibility = View.VISIBLE
                        resource?.data?.results?.let { movieList ->
                            nowPlayingMoviesAdapter.addMovies(movieList)
                            nowPlayingMoviesAdapter.notifyDataSetChanged()
                        }

                    }
                    Status.ERROR -> {
                        Snackbar.make(recyclerViewHorizontal, "Error Occurred", Snackbar.LENGTH_SHORT).show()
                    }
                    Status.LOADING -> {

                    }
                }
            }
        }
        mainViewModel.popularMovies.observe(this, popularMovieObserver)
        mainViewModel.nowPlayingMovies.observe(this, nowPlayingMoviesObserver)
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
}
