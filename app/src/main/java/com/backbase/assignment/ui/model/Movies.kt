package com.backbase.assignment.ui.model

import com.google.gson.annotations.SerializedName

data class Movies(
    var results: List<MovieDetails>? = null,
    var page: Int? = null,
    var totalResults: Int? = null,
    var dates: Dates? = null,
    @SerializedName("total_pages")
    var totalPages: Int? = null
)


