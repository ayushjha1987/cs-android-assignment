package com.backbase.assignment.ui.model


data class Dates(
	var maximum: String? = null,
	var minimum: String? = null
)