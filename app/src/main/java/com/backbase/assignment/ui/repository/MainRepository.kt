package com.backbase.assignment.ui.repository

import com.backbase.assignment.ui.api.ApiHelper
import com.backbase.assignment.ui.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    @ExperimentalCoroutinesApi
    fun getNowPlayingMovies() = flow {
        emit(Resource.loading(null))
        val response = apiHelper.getNowPlayingMovies()
        if (response != null) {
            emit(Resource.success(response))
        } else {
            emit(Resource.error(null, "Http Error!"))
        }
    }

    @ExperimentalCoroutinesApi
    fun getPopularMovies(pageSize: Int) = flow {
        emit(Resource.loading(null))
        val response = apiHelper.getPopularMovies(pageSize)
        if (response != null) {
            emit(Resource.success(response))
        } else {
            emit(Resource.error(null, "Http Error!"))
        }
    }

    @ExperimentalCoroutinesApi
    fun getMovieDetails(id: Int) = flow {
        emit(Resource.loading(null))
        val response = apiHelper.getMovieDetails(id)
        if (response != null) {
            emit(Resource.success(response))
        } else {
            emit(Resource.error(null, "Http Error!"))
        }
    }

}