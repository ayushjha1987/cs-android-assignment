package com.backbase.assignment.ui.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}