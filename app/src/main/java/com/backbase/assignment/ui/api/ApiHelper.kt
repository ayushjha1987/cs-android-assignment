package com.backbase.assignment.ui.api

import javax.inject.Inject

class ApiHelper @Inject constructor(private val apiService: ApiService) {

    suspend fun getNowPlayingMovies() = apiService.getNowPlayingMovies()
    suspend fun getPopularMovies(pageSize: Int) = apiService.getPopularMovies(pageSize)
    suspend fun getMovieDetails(id: Int) = apiService.getMovieDetails(id)
}