package com.backbase.assignment.ui.activity.details

import android.os.Bundle
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.backbase.assignment.R
import com.backbase.assignment.ui.model.DetailsScreen
import com.backbase.assignment.ui.utils.Resource
import com.backbase.assignment.ui.utils.Status
import com.backbase.assignment.ui.viewmodel.DetailsViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.details_screen.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


@AndroidEntryPoint
class DetailsActivity : AppCompatActivity(), CoroutineScope {

    private var movieId: Int = 0
    private val detailsViewModel: DetailsViewModel by viewModels()
    private var job: Job = Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details_screen)
        movieId = intent.getIntExtra("MOVIE_ID", 0)
        setupObservers()
    }

    @ExperimentalCoroutinesApi
    override fun onResume() {
        super.onResume()
        launch {
            detailsViewModel.getMovieDetails(movieId)
        }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private fun setupObservers() {
        val popularMovieObserver = Observer<Resource<DetailsScreen>> {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Glide.with(movieImageView)
                            .load("https://image.tmdb.org/t/p/original/${resource.data?.posterPath}")
                            .diskCacheStrategy(
                                DiskCacheStrategy.RESOURCE
                            ).into(movieImageView)

                        overviewText.text = resource.data?.overview
                        movieTitle.text = resource.data?.original_title
                        val subTitleText = resource.data?.release_date + " - " + getRunTime(
                            resource.data?.runtime ?: 0
                        )
                        subTitle.text = subTitleText
                        resource.data?.let { data -> setGenres(resource, data) }
                    }
                    Status.ERROR -> {

                    }
                    Status.LOADING -> {

                    }
                }
            }
        }
        detailsViewModel.movieDetail.observe(this, popularMovieObserver)
    }

    private fun setGenres(resource: Resource<DetailsScreen>, data: DetailsScreen) {
        if (resource.data?.genres?.isNotEmpty() == true) {
            genreLayout.removeAllViews()
            for (genre in data.genres) {
                val params: LinearLayout.LayoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                params.setMargins(10, 10, 10, 10)
                val rowTextView = layoutInflater.inflate(R.layout.genre_text_view, null) as TextView
                params.setMargins(10, 10, 10, 10)
                rowTextView.layoutParams = params

                rowTextView.text = genre.name
                genreLayout.addView(rowTextView)
            }
        }
    }

    private fun getRunTime(time: Int): String {
        val hours: Int = time / 60 //since both are ints, you get an int
        val minutes: Int = time % 60
        return hours.toString() + "h " + minutes.toString() + "m"
    }
}
